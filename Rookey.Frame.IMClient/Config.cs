﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Reflection;
using System.IO;

namespace Rookey.Frame.IMClient
{
	class Config
	{
		static Config m_Instance = new Config();

		public static Config Instance { get { return m_Instance; } }

		string m_Path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Setting.conf";
		string m_ServiceUrl = "";

		public String ServiceUrl { get { return m_ServiceUrl; } }
		
		private Config()
		{
		}

		public void Load()
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(m_Path);

				m_ServiceUrl = (doc.DocumentElement.GetElementsByTagName("ServiceUrl")[0] as XmlElement).InnerText;
			}
			catch
			{
				throw new Exception("读取配置文件失败!");
			}
		}
	}
}
