﻿namespace Rookey.Frame.IMClient
{
	partial class Window
	{
		/// <summary>
		/// 必需的设计器变量。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 清理所有正在使用的资源。
		/// </summary>
		/// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows 窗体设计器生成的代码

		/// <summary>
		/// 设计器支持所需的方法 - 不要
		/// 使用代码编辑器修改此方法的内容。
		/// </summary>
		private void InitializeComponent()
		{
			this.browser1 = new Rookey.Frame.IMClient.Browser();
			this.loadingTip1 = new Rookey.Frame.IMClient.LoadingTip();
			this.SuspendLayout();
			// 
			// m_BtnClose
			// 
			this.m_BtnClose.Location = new System.Drawing.Point(543, 12);
			this.m_BtnClose.Click += new System.EventHandler(this.m_BtnClose_Click);
			// 
			// m_BtnMin
			// 
			this.m_BtnMin.Location = new System.Drawing.Point(480, 12);
			// 
			// m_BtnRestore
			// 
			this.m_BtnRestore.Location = new System.Drawing.Point(501, 12);
			// 
			// m_BtnMax
			// 
			this.m_BtnMax.Location = new System.Drawing.Point(522, 12);
			// 
			// browser1
			// 
			this.browser1.AllowWebBrowserDrop = false;
			this.browser1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.browser1.Location = new System.Drawing.Point(12, 29);
			this.browser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.browser1.Name = "browser1";
			this.browser1.ScrollBarsEnabled = false;
			this.browser1.Size = new System.Drawing.Size(520, 424);
			this.browser1.TabIndex = 0;
			this.browser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.browser1_DocumentCompleted);
			// 
			// loadingTip1
			// 
			this.loadingTip1.Location = new System.Drawing.Point(55, 13);
			this.loadingTip1.Name = "loadingTip1";
			this.loadingTip1.Size = new System.Drawing.Size(75, 23);
			this.loadingTip1.TabIndex = 5;
			this.loadingTip1.Text = "loadingTip1";
			// 
			// Window
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(544, 465);
			this.Controls.Add(this.loadingTip1);
			this.Controls.Add(this.browser1);
			this.Name = "Window";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Window_FormClosed);
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Window_FormClosing);
			this.Controls.SetChildIndex(this.m_LabelText, 0);
			this.Controls.SetChildIndex(this.m_BtnClose, 0);
			this.Controls.SetChildIndex(this.m_BtnMin, 0);
			this.Controls.SetChildIndex(this.m_BtnRestore, 0);
			this.Controls.SetChildIndex(this.m_BtnMax, 0);
			this.Controls.SetChildIndex(this.browser1, 0);
			this.Controls.SetChildIndex(this.loadingTip1, 0);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private Browser browser1;
		private LoadingTip loadingTip1;
	}
}

