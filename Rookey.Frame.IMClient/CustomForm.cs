﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Rookey.Frame.IMClient
{
	using Properties;

	public partial class CustomForm : Form
	{
		BkImage _bkImage = new BkImage(
			Resources.ImageBG,
			new Rectangle(0, 0, Resources.ImageBG.Width, Resources.ImageBG.Height),
			30, 20, 30, 10
		);

		BkImage[] m_BtnBks = new BkImage[12];

		protected override CreateParams CreateParams
		{
			get
			{
				CreateParams cp = base.CreateParams;
				unchecked
				{
					cp.Style |= (int)(0x00080000 | 0x00010000 | 0x00020000);
				}
				return cp;
			}
		}

		public CustomForm()
		{
			InitializeComponent();

			for (int i = 0; i < 12; i++)
			{
				m_BtnBks[i] = new BkImage(Resources.ImageBtn, new Rectangle(i * 15, 0, 15, 15));
			}

			m_BtnClose.SetBkImage(m_BtnBks[0], m_BtnBks[4], m_BtnBks[8]);
			m_BtnMax.SetBkImage(m_BtnBks[1], m_BtnBks[5], m_BtnBks[9]);
			m_BtnMin.SetBkImage(m_BtnBks[2], m_BtnBks[6], m_BtnBks[10]);
			m_BtnRestore.SetBkImage(m_BtnBks[3], m_BtnBks[7], m_BtnBks[11]);

			m_BtnClose.Cursor = Cursors.Default;
			m_BtnMax.Cursor = Cursors.Default;
			m_BtnMin.Cursor = Cursors.Default;
			m_BtnRestore.Cursor = Cursors.Default;

		}

		public void Init(
			int left, int top, int width, int height,
			int borderWidth, int titleHeight,
			bool isResizable, bool hasMin, bool hasMax,
			int minWidth, int minHeight,
			string text
		)
		{
			Bounds = new Rectangle(left, top, width, height);

			_borderWidth = borderWidth;
			_titleHeight = titleHeight;

			_minHeight = minHeight;
			_minWidth = minWidth;

			_isResizable = isResizable;

			Text = text;

			int bl = (width - m_BtnClose.Width - _borderWidth);
			m_BtnClose.Location = new Point(bl, _borderWidth + (_titleHeight - m_BtnClose.Height) / 2);

			if (hasMax)
			{
				bl -= m_BtnMax.Width + 2;
				m_BtnMax.Location = new Point(bl, _borderWidth + (_titleHeight - m_BtnClose.Height) / 2);
				m_BtnRestore.Location = new Point(bl, _borderWidth + (_titleHeight - m_BtnClose.Height) / 2);
			}

			if (hasMin)
			{
				bl -= m_BtnMin.Width + 2;
				m_BtnMin.Location = new Point(bl, _borderWidth + (_titleHeight - m_BtnClose.Height) / 2);
			}

			m_BtnRestore.Visible = false;
			m_BtnMax.Visible = hasMax;
			m_BtnMin.Visible = hasMin;

			m_LabelText.Text = text;
			m_LabelText.BackColor = Color.FromArgb(0, Color.White);
			m_LabelText.ForeColor = Color.FromArgb(0x15, 0x42, 0x8B);
			m_LabelText.Location = new Point(30, _borderWidth + (titleHeight - m_LabelText.Height) / 2);

			this.MinimumSize = new Size(_minWidth, _minHeight);
			this.MaximumSize = new Size(SystemInformation.WorkingArea.Width, SystemInformation.WorkingArea.Height);
		}

		private bool _isResizable = true;

		public bool IsResizable
		{
			get { return _isResizable; }
		}

		protected Int32 _borderWidth = 6, _titleHeight = 18;

		public Int32 TitleHeight
		{
			get { return _titleHeight; }
		}

		public Int32 BorderWidth
		{
			get { return _borderWidth; }
		}

		Int32 _minWidth = 400, _minHeight = 300;

		protected override void OnPaintBackground(PaintEventArgs e)
		{
			_bkImage.Draw(e.Graphics, Bounds);
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
		}

		int _preX, _preY, _preLeft, _preTop, _preWidth, _preHeight;
		int _pos;

		protected override void OnMouseDown(MouseEventArgs e)
		{
			_preX = Left + e.X;
			_preY = Top + e.Y;
			_preLeft = Left;
			_preTop = Top;
			_preWidth = Width;
			_preHeight = Height;

			_pos = GetPos(e.X, e.Y);

			SetCursor();

			Capture = true;

		}

		private void m_LabelText_MouseDown(object sender, MouseEventArgs e)
		{
			_preX = Left + e.X + m_LabelText.Left;
			_preY = Top + e.Y + m_LabelText.Top;
			_preLeft = Left;
			_preTop = Top;
			_preWidth = Width;
			_preHeight = Height;

			_pos = 11;

			SetCursor();

			Capture = true;
		}

		private int GetPos(int x, int y)
		{

			int x1 = _borderWidth, x2 = Width - _borderWidth, x3 = Width;
			int y1 = _borderWidth, y2 = Height - _borderWidth, y3 = Height;

			if (x < x1) _pos = 0;
			else if (x < x2) _pos = 1;
			else _pos = 2;

			if (y < y1) _pos += 0;
			else if (y < y2) _pos += 10;
			else _pos += 20;

			if (_pos == 11 && y > y1 + _titleHeight) _pos = -1;

			return _pos;
		}

		protected override void OnMouseMove(MouseEventArgs e)
		{
			if (Capture)
			{
				int difX = Left + e.X - _preX;
				int difY = Top + e.Y - _preY;

				int newLeft = Left, newTop = Top, newWidth = Width, newHeight = Height;

				if (_pos == 11)
				{
					Location = new Point(_preLeft + difX, _preTop + difY);
				}
				else
				{
					if (_isResizable)
					{
						switch (_pos)
						{
						case 0:
							newLeft = _preLeft + difX;
							newTop = _preTop + difY;
							newWidth = _preWidth - difX;
							newHeight = _preHeight - difY;
							if (newWidth < _minWidth)
							{
								newLeft -= _minWidth - newWidth;
								newWidth = _minWidth;
							}
							if (newHeight < _minHeight)
							{
								newTop -= _minHeight - newHeight;
								newHeight = _minHeight;
							}
							break;
						case 1:
							newTop = _preTop + difY;
							newHeight = _preHeight - difY;
							if (newHeight < _minHeight)
							{
								newTop -= _minHeight - newHeight;
								newHeight = _minHeight;
							}
							break;
						case 2:
							newTop = _preTop + difY;
							newWidth = _preWidth + difX;
							newHeight = _preHeight - difY;
							if (newWidth < _minWidth)
							{
								newWidth = _minWidth;
							}
							if (newHeight < _minHeight)
							{
								newTop -= _minHeight - newHeight;
								newHeight = _minHeight;
							}
							break;
						case 10:
							newLeft = _preLeft + difX;
							newWidth = _preWidth - difX;
							if (newWidth < _minWidth)
							{
								newLeft -= _minWidth - newWidth;
								newWidth = _minWidth;
							}
							break;
						case 12:
							newWidth = _preWidth + difX;
							if (newWidth < _minWidth)
							{
								newWidth = _minWidth;
							}
							break;
						case 20:
							newLeft = _preLeft + difX;
							newWidth = _preWidth - difX;
							newHeight = _preHeight + difY;
							if (newWidth < _minWidth)
							{
								newLeft -= _minWidth - newWidth;
								newWidth = _minWidth;
							}
							if (newHeight < _minHeight)
							{
								newHeight = _minHeight;
							}
							break;
						case 21:
							newHeight = _preHeight + difY;
							if (newHeight < _minHeight)
							{
								newHeight = _minHeight;
							}
							break;
						case 22:
							newWidth = _preWidth + difX;
							newHeight = _preHeight + difY;
							if (newWidth < _minWidth)
							{
								newWidth = _minWidth;
							}
							if (newHeight < _minHeight)
							{
								newHeight = _minHeight;
							}
							break;
						}

						Bounds = new Rectangle(newLeft, newTop, newWidth, newHeight);
						Refresh();
					}
				}
			}
			else
			{
				_pos = GetPos(e.X, e.Y);
				SetCursor();
			}
		}

		private void SetCursor()
		{
			if (_pos == 11)
			{
				Cursor = Cursors.SizeAll;
			}
			else
			{
				if (_isResizable)
				{

					switch (_pos)
					{
					case 0:
						Cursor = Cursors.SizeNWSE;
						break;
					case 1:
						Cursor = Cursors.SizeNS;
						break;
					case 2:
						Cursor = Cursors.SizeNESW;
						break;
					case 10:
						Cursor = Cursors.SizeWE;
						break;
					case 12:
						Cursor = Cursors.SizeWE;
						break;
					case 20:
						Cursor = Cursors.SizeNESW;
						break;
					case 21:
						Cursor = Cursors.SizeNS;
						break;
					case 22:
						Cursor = Cursors.SizeNWSE;
						break;
					default:
						Cursor = Cursors.Default;
						break;
					}
				}
				else
				{
					Cursor = Cursors.Default;
				}
			}
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			Capture = false;
		}

		private void m_BtnMin_Click(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Minimized;
		}

		private void m_BtnMax_Click(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Maximized;
			m_BtnMax.Visible = false;
			m_BtnRestore.Visible = true;
			Refresh();
		}

		private void m_BtnRestore_Click(object sender, EventArgs e)
		{
			WindowState = FormWindowState.Normal;
			m_BtnMax.Visible = true;
			m_BtnRestore.Visible = false;
			Refresh();
		}

		private void CustomForm_Shown(object sender, EventArgs e)
		{
		}

		private void CustomForm_VisibleChanged(object sender, EventArgs e)
		{
		}

		private void CustomForm_Resize(object sender, EventArgs e)
		{
			Refresh();
		}
	}
}
