﻿namespace Rookey.Frame.IMClient
{
	partial class CustomForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_LabelText = new System.Windows.Forms.Label();
			this.m_BtnMax = new Rookey.Frame.IMClient.CustomButton();
			this.m_BtnRestore = new Rookey.Frame.IMClient.CustomButton();
			this.m_BtnMin = new Rookey.Frame.IMClient.CustomButton();
			this.m_BtnClose = new Rookey.Frame.IMClient.CustomButton();
			this.SuspendLayout();
			// 
			// m_LabelText
			// 
			this.m_LabelText.AutoSize = true;
			this.m_LabelText.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
			this.m_LabelText.Location = new System.Drawing.Point(12, 9);
			this.m_LabelText.Name = "m_LabelText";
			this.m_LabelText.Size = new System.Drawing.Size(0, 12);
			this.m_LabelText.TabIndex = 4;
			this.m_LabelText.MouseDown += new System.Windows.Forms.MouseEventHandler(this.m_LabelText_MouseDown);
			// 
			// m_BtnMax
			// 
			this.m_BtnMax.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_BtnMax.Location = new System.Drawing.Point(485, 12);
			this.m_BtnMax.Name = "m_BtnMax";
			this.m_BtnMax.Size = new System.Drawing.Size(15, 15);
			this.m_BtnMax.TabIndex = 3;
			this.m_BtnMax.Click += new System.EventHandler(this.m_BtnMax_Click);
			// 
			// m_BtnRestore
			// 
			this.m_BtnRestore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_BtnRestore.Location = new System.Drawing.Point(464, 12);
			this.m_BtnRestore.Name = "m_BtnRestore";
			this.m_BtnRestore.Size = new System.Drawing.Size(15, 15);
			this.m_BtnRestore.TabIndex = 2;
			this.m_BtnRestore.Click += new System.EventHandler(this.m_BtnRestore_Click);
			// 
			// m_BtnMin
			// 
			this.m_BtnMin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_BtnMin.Location = new System.Drawing.Point(443, 12);
			this.m_BtnMin.Name = "m_BtnMin";
			this.m_BtnMin.Size = new System.Drawing.Size(15, 15);
			this.m_BtnMin.TabIndex = 1;
			this.m_BtnMin.Click += new System.EventHandler(this.m_BtnMin_Click);
			// 
			// m_BtnClose
			// 
			this.m_BtnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.m_BtnClose.Location = new System.Drawing.Point(506, 12);
			this.m_BtnClose.Name = "m_BtnClose";
			this.m_BtnClose.Size = new System.Drawing.Size(15, 15);
			this.m_BtnClose.TabIndex = 0;
			// 
			// CustomForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(533, 384);
			this.Controls.Add(this.m_LabelText);
			this.Controls.Add(this.m_BtnMax);
			this.Controls.Add(this.m_BtnRestore);
			this.Controls.Add(this.m_BtnMin);
			this.Controls.Add(this.m_BtnClose);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "CustomForm";
			this.Text = "Form1";
			this.Shown += new System.EventHandler(this.CustomForm_Shown);
			this.VisibleChanged += new System.EventHandler(this.CustomForm_VisibleChanged);
			this.Resize += new System.EventHandler(this.CustomForm_Resize);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		protected CustomButton m_BtnClose;
		protected CustomButton m_BtnMin;
		protected CustomButton m_BtnRestore;
		protected CustomButton m_BtnMax;
		protected System.Windows.Forms.Label m_LabelText;
	}
}