﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Rookey.Frame.IMClient
{
    public partial class FullScreenForm : Form
    {
        private Rectangle rectSelected = Rectangle.Empty;
        private Bitmap screen, origScreen;
        private Bitmap resultBmp = null;
        private bool _isCapture = false;
        private Point _prePos;

        public FullScreenForm(Bitmap screen)
        {

            InitializeComponent();

            int width = Screen.PrimaryScreen.Bounds.Width;
            int height = Screen.PrimaryScreen.Bounds.Height;

            Bitmap bitmap = new Bitmap(width, height);
            Graphics graph = Graphics.FromImage(bitmap);
            graph.DrawImage(screen, 0, 0);
            graph.FillRectangle(new SolidBrush(Color.FromArgb(80, 0, 0, 0)), new Rectangle(0, 0, width, height));

            this.screen = bitmap;
            this.origScreen = screen;
            this.Bounds = new Rectangle(0, 0, width, height);
            this.DoubleBuffered = true;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (!_isCapture && rectSelected == Rectangle.Empty)
                {
                    _isCapture = true;
                    rectSelected = Rectangle.Empty;
                    rectSelected.Location = e.Location;
                    _prePos = e.Location;
                    this.Invalidate();
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (rectSelected == Rectangle.Empty)
                {
                    this.DialogResult = DialogResult.Cancel;
                }
                else
                {
                    rectSelected = Rectangle.Empty;
                    this.Invalidate();
                }
            }

        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _isCapture)
            {
                if (e.X > _prePos.X)
                {
                    rectSelected.Width = e.X - _prePos.X;
                }
                else
                {
                    rectSelected.Width = _prePos.X - e.X;
                    rectSelected.X = e.X;
                }
                if (e.Y > _prePos.Y)
                {
                    rectSelected.Height = e.Y - _prePos.Y;
                }
                else
                {
                    rectSelected.Height = _prePos.Y - e.Y;
                    rectSelected.Y = e.Y;
                }
                this.Invalidate();
            }

        }

        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);
            try
            {
                resultBmp = new Bitmap(rectSelected.Width, rectSelected.Height);
                using (Graphics g = Graphics.FromImage(resultBmp))
                {
                    g.DrawImage(origScreen, new Rectangle(0, 0, rectSelected.Width, rectSelected.Height), rectSelected, GraphicsUnit.Pixel);
                }
            }
            catch
            {
            }
            this.DialogResult = DialogResult.OK;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                try
                {
                    if (_isCapture)
                    {
                        _isCapture = false;
                        if (e.X > _prePos.X)
                        {
                            rectSelected.Width = e.X - _prePos.X;
                        }
                        else
                        {
                            rectSelected.Width = _prePos.X - e.X;
                            rectSelected.X = e.X;
                        }
                        if (e.Y > _prePos.Y)
                        {
                            rectSelected.Height = e.Y - _prePos.Y;
                        }
                        else
                        {
                            rectSelected.Height = _prePos.Y - e.Y;
                            rectSelected.Y = e.Y;
                        }
                        this.Invalidate();
                    }
                }
                catch
                {
                }
            }

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(screen, 0, 0);
            g.DrawImage(origScreen, rectSelected, rectSelected, GraphicsUnit.Pixel);
            if (rectSelected == Rectangle.Empty)
            {
                g.DrawString("双击完成截图，右键取消，Esc退出", new Font("宋体", 12), Brushes.Red, 0, 0);
            }
            else
            {
                g.DrawString("双击完成截图，右键取消，Esc退出", new Font("宋体", 12), Brushes.Red, rectSelected.X, rectSelected.Y - 20);
            }
            g.DrawRectangle(new Pen(Color.Blue), rectSelected);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.DialogResult = DialogResult.Cancel;
            }

        }

        private void PaintRectangle()
        {
            

        }

        public Bitmap ResultBitmap
        {
            get { return resultBmp; }
        }

    }
}
