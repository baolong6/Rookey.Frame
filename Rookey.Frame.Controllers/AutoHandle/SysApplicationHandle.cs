﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Rookey.Frame.Common;
using Rookey.Frame.Common.Model;
using Rookey.Frame.Operate.Base;

namespace Rookey.Frame.Controllers.AutoHandle
{
    /// <summary>
    /// 系统应用程序处理类
    /// </summary>
    public static class SysApplicationHandle
    {
        /// <summary>
        /// 应用程序启动
        /// </summary>
        public static void Application_Start(HttpApplicationState application)
        {
            try
            {
                //向各数据库注册存储过程
                SystemOperate.RegStoredProcedure();
                //在当前数据库中自动注册外部链接数据库服务器
                SystemOperate.RegCrossDbServer();
            }
            catch { }
        }

        /// <summary>
        /// 应用程序结束
        /// </summary>
        public static void Application_End()
        {
            try
            {
            }
            catch { }
        }
    }
}
