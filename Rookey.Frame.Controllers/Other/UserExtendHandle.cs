﻿using Rookey.Frame.Base;
using Rookey.Frame.Base.Set;
using Rookey.Frame.Operate.Base;
using Rookey.Frame.Model.OrgM;
using System.Linq;
using System.Collections.Generic;
using Rookey.Frame.Base.User;
using System;

namespace Rookey.Frame.Controllers.Other
{
    /// <summary>
    /// 用户扩展处理类
    /// </summary>
    public static class UserExtendHandle
    {
        /// <summary>
        /// 获取用户扩展信息
        /// </summary>
        /// <param name="o"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static UserExtendBase GetUserExtendObject(object o, EventUserArgs e)
        {
            if (e.CurrUser != null && e.CurrUser.EmpId.HasValue && e.CurrUser.EmpId.Value!=Guid.Empty)
            {
                List<EmpExtendInfo> empExtends = new List<EmpExtendInfo>();
                List<Guid> companyIds = OrgMOperate.GetEmpCompanys(e.CurrUser.EmpId.Value).Select(x => x.Id).ToList();
                if (companyIds.Count > 0)
                {
                    foreach (Guid companyId in companyIds)
                    {
                        OrgM_Dept mainDept = OrgMOperate.GetEmpMainDept(e.CurrUser.EmpId.Value, companyId);
                        OrgM_Duty mainDuty = OrgMOperate.GetEmpMainDuty(e.CurrUser.EmpId.Value, companyId);
                        List<OrgM_Dept> partimeDepts = OrgMOperate.GetEmpPartTimeDepts(e.CurrUser.EmpId.Value, companyId);
                        List<OrgM_Duty> partimeDutys = OrgMOperate.GetEmpPartTimeDutys(e.CurrUser.EmpId.Value, companyId);
                        empExtends.Add(new EmpExtendInfo()
                        {
                            CompanyId = companyId,
                            DeptId = mainDept.Id,
                            DeptName = mainDept.Name,
                            DutyId = mainDuty.Id,
                            DutyName = mainDuty.Name,
                            PartimeDeptIds = partimeDepts != null && partimeDepts.Count > 0 ? partimeDepts.Select(x => x.Id).ToList() : null,
                            PartimeDeptNames = partimeDepts != null && partimeDepts.Count > 0 ? partimeDepts.Select(x => x.Name).ToList() : null,
                            PartimeDutyIds = partimeDutys != null && partimeDutys.Count > 0 ? partimeDutys.Select(x => x.Id).ToList() : null,
                            PartimeDutyNames = partimeDutys != null && partimeDutys.Count > 0 ? partimeDutys.Select(x => x.Name).ToList() : null
                        });
                    }
                    return new UserExtendInfo()
                    {
                        EmpExtend = empExtends
                    };
                }
            }
            return null;
        }
    }
}
