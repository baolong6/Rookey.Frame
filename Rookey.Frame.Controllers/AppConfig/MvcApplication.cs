﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using Rookey.Frame.Base;
using Rookey.Frame.Common;
using System.Web.Security;
using Rookey.Frame.AutoProcess;
using Rookey.Frame.Controllers.AutoHandle;
using FluentValidation.Mvc;
using Rookey.Frame.Controllers.Other;
using FluentValidation.Attributes;
using Rookey.Frame.Base.Set;
using Rookey.Frame.Operate.Base;

namespace Rookey.Frame.Controllers.AppConfig
{
    /// <summary>
    /// MVC应用程序类
    /// </summary>
    public class MvcApplication : HttpApplication
    {
        /// <summary>
        /// 应用程序启动
        /// </summary>
        public void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //启动自动处理程序
            AutoProcessTask.EventAfterExecute += new EventHandler(SysAutoHandle.SysBackgroundTaskAdd);
            AutoProcessTask.Execute();
            //用户扩展对象
            UserExtendEventHandler.BindUserExtendEvent += new UserExtendEventHandler.EventUserExtend(UserExtendHandle.GetUserExtendObject);
            //自定义应用程序启动
            SysApplicationHandle.Application_Start(this.Application);
            //验证配置
            ConfigureFluentValidation();
            //注册视图访问规则
            RegisterView();
            //加载所有启用缓存的模块数据
            if (GlobalSet.IsStartLoadCache)
            {
                SystemOperate.LoadAllModuleCache();
            }
        }

        /// <summary>
        /// 注册视图访问规则
        /// </summary>
        private void RegisterView()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new MyViewEngine());
        }

        /// <summary>
        /// 应用程序结束
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Application_End(object sender, EventArgs e)
        {
            //自定义应用程序结束
            SysApplicationHandle.Application_End();
        }

        /// <summary>
        /// FluentValidation验证设置
        /// </summary>
        private void ConfigureFluentValidation()
        {
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;
            ModelValidatorProviders.Providers.Add(new FluentValidationModelValidatorProvider(new AttributedValidatorFactory()));
        }

        /// <summary>
        /// 应用程序请求开始
        /// </summary>
        /// <param name="sender">发送对象</param>
        /// <param name="e">事件参数</param>
        public void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            ApplicationObject.CurrentHttpContext = app.Context;
            HttpCookie cookie = app.Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                cookie.Expires = DateTime.Now.AddMinutes(UserInfo.ACCOUNT_EXPIRATION_TIME);
                Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// 应用程序结束请求
        /// </summary>
        public void Application_EndRequest()
        {
        }

        /// <summary>
        /// 应用程序认证请求
        /// </summary>
        /// <param name="sender">发送对象</param>
        /// <param name="e">事件参数</param>
        public void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            HttpApplication app = (HttpApplication)sender;
            FormsPrincipal.TrySetUserInfo(app.Context);
        }
    }
}
