﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace Rookey.Frame.Common
{
    /// <summary>
    /// Web辅助处理类
    /// </summary>
    public static class WebHelper
    {
        /// <summary>
        /// 获取客户端IP
        /// </summary>
        /// <param name="request">请求对象</param>
        /// <returns></returns>
        public static string GetClientIP(HttpRequestBase request)
        {
            string clientIp = string.Empty;
            if (request.ServerVariables["HTTP_VIA"] != null) //使用代理
            {
                clientIp = request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString(); // 返回真实的客户端IP 
            }
            else// 没有使用代理时获取客户端IP 
            {
                clientIp = request.ServerVariables["REMOTE_ADDR"].ToString(); //当不能获取客户端IP时,将获取客户端代理IP. 
            }
            return clientIp;
        }

        /// <summary>
        /// 获取配置文件
        /// </summary>
        /// <param name="configName">配置文件名，带文件扩展名</param>
        /// <returns></returns>
        public static string GetConfigFilePath(string configName)
        {
            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            if (!basePath.EndsWith("\\"))
            {
                basePath += "\\";
            }
            string xmlPath = basePath + string.Format("Config\\{0}", configName);
            if (!System.IO.File.Exists(xmlPath)) //文件不存在
                return string.Empty;
            return xmlPath;
        }

        /// <summary>
        /// 获取webapi请求对象中的传统request对象
        /// </summary>
        /// <param name="requestMessage">webapi请求对象</param>
        /// <returns></returns>
        public static HttpRequestBase GetContextRequest(HttpRequestMessage requestMessage)
        {
            if (requestMessage != null)
            {
                HttpContextBase context = (HttpContextBase)requestMessage.Properties["MS_HttpContext"]; //获取传统context
                return context.Request; //定义传统request对象
            }
            return null;
        }
    }
}
