﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Rookey.Frame.Common
{
    /// <summary>
    /// 全局对象
    /// </summary>
    public static class ApplicationObject
    {
        private static HttpContext _currentHttpContext;

        /// <summary>
        /// 当前上下文对象
        /// </summary>
        public static HttpContext CurrentHttpContext
        {
            get 
            {
                if (_currentHttpContext == null)
                    return HttpContext.Current;
                return _currentHttpContext;
            }
            set { _currentHttpContext = value; }
        }
    }
}
