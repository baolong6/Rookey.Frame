﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Rookey.Frame.Common
{
    public static class JsonHelper
    {
        /// <summary>
        /// 反序列化为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string json) where T : class
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// 序列化到json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static string Serialize<T>(T entity) where T : class
        {
            IsoDateTimeConverter timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(entity, Newtonsoft.Json.Formatting.Indented, timeFormat);
        }

        /// <summary>
        /// 将datatable转换为json  
        /// </summary>
        /// <param name="dt">Dt</param>
        /// <returns>JSON字符串</returns>
        public static string DataTableToJson(System.Data.DataTable dt)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            System.Collections.ArrayList dic = new System.Collections.ArrayList();
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                System.Collections.Generic.Dictionary<string, object> drow = new System.Collections.Generic.Dictionary<string, object>();
                foreach (System.Data.DataColumn dc in dt.Columns)
                {
                    drow.Add(dc.ColumnName, dr[dc.ColumnName]);
                }
                dic.Add(drow);

            }
            //序列化  
            return js.Serialize(dic);
        }

        /// <summary>    
        /// 将获取的Json数据转换为DataTable    
        /// </summary>    
        /// <param name="strJson">Json字符串</param>   
        /// <returns></returns>    
        public static System.Data.DataTable JsonToDataTable(string json)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            ArrayList dic = js.Deserialize<ArrayList>(json);
            System.Data.DataTable dt = new System.Data.DataTable();
            if (dic.Count > 0)
            {
                foreach (Dictionary<string, object> drow in dic)
                {
                    if (dt.Columns.Count == 0)
                    {
                        foreach (string key in drow.Keys)
                        {
                            dt.Columns.Add(key, drow[key].GetType());
                        }
                    }
                    System.Data.DataRow row = dt.NewRow();
                    foreach (string key in drow.Keys)
                    {
                        row[key] = drow[key];
                    }
                    dt.Rows.Add(row);
                }
            }
            return dt; 
        }
    }

    public class MyJsonHelper<T> where T : class
    {
        /// <summary>
        /// 反序列化为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public T Deserialize(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// 反序列化为对象集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public List<T> CollectDeserialize(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(json);
        }

        /// <summary>
        /// 序列化到json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public string Serialize(T entity)
        {
            IsoDateTimeConverter timeFormat = new IsoDateTimeConverter();
            timeFormat.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            return JsonConvert.SerializeObject(entity, Newtonsoft.Json.Formatting.Indented, timeFormat);
        }
    }
}
