﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/
using Rookey.Frame.EntityBase;

namespace Rookey.Frame.Model
{
    /// <summary>
    /// 系统监控实体基类
    /// </summary>
    public class BaseMonitorEntity : BaseEntity
    {
    }
}
