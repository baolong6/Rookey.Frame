﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.EntityBase;
using Rookey.Frame.EntityBase.Attr;
using ServiceStack.DataAnnotations;
using System;
using System.Collections.Generic;

namespace Rookey.Frame.Model.Bpm
{
    /// <summary>
    /// 流程信息
    /// </summary>
    [ModuleConfig(Name = "流程信息", PrimaryKeyFields = "Name", TitleKey = "Name", Sort = 71, StandardJsFolder = "Bpm")]
    public class Bpm_WorkFlow : BaseBpmEntity
    {
        /// <summary>
        /// 流程名称
        /// </summary>
        [FieldConfig(Display = "流程名称", IsUnique = true, IsRequired = true, RowNum = 1, ColNum = 1, HeadSort = 1)]
        [StringLength(200)]
        public string Name { get; set; }

        /// <summary>
        /// 显示名称
        /// </summary>
        [FieldConfig(Display = "显示名称", RowNum = 1, ColNum = 2, DefaultValue = "{Name}", HeadSort = 2)]
        [StringLength(200)]
        public string DisplayName { get; set; }

        /// <summary>
        /// 所属分类Id
        /// </summary>
        [FieldConfig(Display = "所属分类", ControlType = (int)ControlTypeEnum.ComboTree, RowNum = 2, ColNum = 1, HeadSort = 3, ForeignModuleName = "流程分类")]
        public Guid? Bpm_FlowClassId { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [Ignore]
        public string Bpm_FlowClassName { get; set; }

        /// <summary>
        /// 关联模块
        /// </summary>
        [FieldConfig(Display = "关联模块", ControlType = (int)ControlTypeEnum.ComboBox, IsRequired = true, RowNum = 2, ColNum = 2, HeadSort = 4, ForeignModuleName = "模块管理")]
        public Guid? Sys_ModuleId { get; set; }

        /// <summary>
        /// 关联模块名称
        /// </summary>
        [Ignore]
        public string Sys_ModuleName { get; set; }

        /// <summary>
        /// 流水号前缀
        /// </summary>
        [FieldConfig(Display = "流水号前缀", RowNum = 3, ColNum = 1, HeadSort = 5)]
        [StringLength(200)]
        public string SerialPrefix { get; set; }

        /// <summary>
        /// 有效开始时间
        /// </summary>
        [FieldConfig(Display = "有效开始时间", ControlType = (int)ControlTypeEnum.DateTimeBox, RowNum = 3, ColNum = 2, HeadSort = 6)]
        public DateTime? ValidStartTime { get; set; }

        /// <summary>
        /// 有效结束时间
        /// </summary>
        [FieldConfig(Display = "有效结束时间", ControlType = (int)ControlTypeEnum.DateTimeBox, RowNum = 4, ColNum = 1, HeadSort = 7)]
        public DateTime? ValidEndTime { get; set; }

        /// <summary>
        /// 节点集合
        /// </summary>
        [Ignore]
        public List<Bpm_WorkNode> WorkNodes { get; set; }

        /// <summary>
        /// 连线集合
        /// </summary>
        [Ignore]
        public List<Bpm_WorkLine> WorkLines { get; set; }
    }
}
