﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.UIOperate
{
    /// <summary>
    /// 常量定义类
    /// </summary>
    internal static class ConstDefine
    {
        /// <summary>
        /// 北方布局高度（LOGO高度）
        /// </summary>
        public const int TOP_NORTH_REGION_HEIGHT = 60;

        /// <summary>
        /// TAB头的高度
        /// </summary>
        public const int TAB_HEAD_HEIGHT = 36;

        /// <summary>
        /// 底部状态栏区域高度
        /// </summary>
        public const int BOTTOM_STATUS_REGON_HEIGHT = 25;

        /// <summary>
        /// 表单控件高度
        /// </summary>
        public const int FORM_CONTROL_HEIGHT = 30;

        /// <summary>
        /// 文本域表单控件高度
        /// </summary>
        public const int FORM_TEXTAREA_CONTROL_HEIGHT = 45;

        /// <summary>
        /// 表单行高
        /// </summary>
        public const int FORM_ROW_HEIGHT = 35;

        /// <summary>
        /// 文本域表单行高
        /// </summary>
        public const int FORM_TEXTAREA_ROW_HEIGHT = 50;

        /// <summary>
        /// 表单panel的padding
        /// </summary>
        public const int FORM_PANEL_PADDING = 10;

        /// <summary>
        /// 弹出框表单最大宽度限制
        /// </summary>
        public const int DIALOG_FORM_MAX_WIDTH = 900;

        /// <summary>
        /// 弹出框表单最大高度限制
        /// </summary>
        public const int DIALOG_FORM_MAX_HEIGHT = 480;

        /// <summary>
        /// 弹出框表单最小宽度
        /// </summary>
        public const int DIALOG_FORM_MIN_WIDTH = 400;

        /// <summary>
        /// 弹出框表单最小高度
        /// </summary>
        public const int DIALOG_FORM_MIN_HEIGHT = 250;

        /// <summary>
        /// 主界面左侧菜单默认宽度
        /// </summary>
        public const int MAIN_LEFT_MENU_WIDTH = 180;

        /// <summary>
        /// 网格左侧菜单默认宽度
        /// </summary>
        public const int GRID_LEFT_MENU_WIDTH = 180;
    }
}
