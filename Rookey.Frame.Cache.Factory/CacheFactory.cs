﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using Rookey.Frame.Cache.Factory.Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Cache.Factory
{
    /// <summary>
    /// 缓存工厂
    /// </summary>
    public static class CacheFactory
    {
        /// <summary>
        /// 锁对象
        /// </summary>
        private static object lockObj = new object();

        /// <summary>
        /// 静态缓存对象
        /// </summary>
        private static Dictionary<CacheProviderType, ICacheProvider> cacheObjects = new Dictionary<CacheProviderType, ICacheProvider>();

        /// <summary>
        /// 创建缓存实例
        /// </summary>
        /// <param name="providerType">缓存类型</param>
        /// <returns></returns>
        public static ICacheProvider GetCacheInstance(CacheProviderType providerType)
        {
            if (cacheObjects.ContainsKey(providerType))
                return cacheObjects[providerType];
            ICacheProvider cacheProvider = new MemoryCacheProvider();
            switch (providerType)
            {
                case CacheProviderType.LOCALMEMORYCACHE:
                    cacheProvider = new MemoryCacheProvider();
                    break;
                case CacheProviderType.MEMCACHEDCACHE:
                    cacheProvider = new MemcachedCacheProvider();
                    break;
                case CacheProviderType.REDIS:
                    cacheProvider = new RedisCacheProvider();
                    break;
            }
            lock (lockObj)
            {
                if (!cacheObjects.ContainsKey(providerType))
                    cacheObjects.Add(providerType, cacheProvider);
            }
            return cacheProvider;
        }
    }
}
