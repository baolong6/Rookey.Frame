﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class ChatForm : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		((HtmlInputHidden)FindControl("data")).Value = Rookey.Frame.IMCore.Utility.RenderHashJson(
			"Peer", Rookey.Frame.IMCore.AccountImpl.Instance.GetUserInfo(Request.Params["peer"]).DetailsJson,
			"User", Rookey.Frame.IMCore.AccountImpl.Instance.GetUserInfo(Rookey.Frame.IMCore.ServerImpl.Instance.GetUserName(Context)).DetailsJson
		);
	}
}
