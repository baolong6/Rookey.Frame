﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Desktop : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
#		if DEBUG
		if (Request.QueryString["auto"] == "true")
		{
			Rookey.Frame.IMCore.ServerImpl.Instance.Login("", Context, "admin", null, false);
		}
#		endif
		if (Request.QueryString["signout"] != null)
		{
			Rookey.Frame.IMCore.ServerImpl.Instance.Logout(Context);
		}
	}
}
