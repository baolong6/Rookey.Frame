﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Configuration;

public partial class Signout : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		Configuration config = WebConfigurationManager.OpenWebConfiguration(Request.ApplicationPath == "/" ? "/IM" : Request.ApplicationPath + "/IM");
		Rookey.Frame.IMCore.ServerImpl.Instance.Logout(Context);
		Response.Redirect(config.AppSettings.Settings["DefaultPage"].Value);
	}
}
