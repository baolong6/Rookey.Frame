﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rookey.Frame.IMWeb
{
    class TempUserInfo
    {
        /// <summary>
        /// 用户Id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 用户别名
        /// </summary>
        public string AliasName { get; set; }
    }
}
