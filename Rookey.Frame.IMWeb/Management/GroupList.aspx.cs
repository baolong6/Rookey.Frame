﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Rookey.Frame.IMCore;

public partial class Management_GroupList : System.Web.UI.Page
{
	static string RowFormat =
	@"
	<tr>
		<td class='headimg'>&nbsp;</td>
		<td class='name'>{1}</td>
		<td class='nickname'>{2}</td>
		<td class='creator'>{3}({4})</td>
		<td class='operation'>
			{10}
			<a href='javascript:Delete({5},{6},{7},{8})'>{9}</a>
		</td>
	</tr>
	";
	CommandCtrl _cmdCtrl;

	protected void Page_Load(object sender, EventArgs e)
	{
		_cmdCtrl = FindControl("CommandCtrl") as CommandCtrl;
		_cmdCtrl.OnCommand += new CommandCtrl.OnCommandDelegate(cmdCtrl_OnCommand);
	}

	private void cmdCtrl_OnCommand(string command, object data)
	{
		try
		{
			string peer = Convert.ToString(data);
			AccountInfo cu = ServerImpl.Instance.GetCurrentUser(Context);

			if (command == "NewGroup")
			{
				Hashtable info = data as Hashtable;
				AccountImpl.Instance.CreateGroup(cu.Name, info["Name"].ToString(), info["Nickname"].ToString());

				_cmdCtrl.State["Action"] = "RefreshFriendsList";
			}
			else if (command == "Exit")
			{
				Int64 id = Convert.ToInt64(data);
				AccountInfo groupInfo = AccountImpl.Instance.GetUserInfo(id);
				AccountImpl.Instance.DeleteFriend(cu.Name, groupInfo.Name);

				//_cmdCtrl.State["Action"] = "RefreshFriendsList";

				string content = Utility.RenderHashJson(
					"Type", "ExitGroupNotify",
					"User", cu,
					"Group", groupInfo
				);

				MessageImpl.Instance.NewMessage(cu.Name, "administrator", content, null);
				MessageImpl.Instance.NewMessage(groupInfo.Name, "administrator", content, null);
			}
			else if (command == "Delete")
			{
				Int64 id = Convert.ToInt64(data);
				AccountInfo groupInfo = AccountImpl.Instance.GetUserInfo(id);
				AccountImpl.Instance.DeleteGroup(groupInfo.Name, cu.Name);

				//_cmdCtrl.State["Action"] = "RefreshFriendsList";

				foreach (string member in groupInfo.Friends)
				{
					AccountInfo memberInfo = AccountImpl.Instance.GetUserInfo(member);
					if (memberInfo.ID == cu.ID) continue;
					string content = Utility.RenderHashJson(
						"Type", "DeletetGroupNotify",
						"Group", groupInfo
					);
					MessageImpl.Instance.NewMessage(memberInfo.Name, "administrator", content, null);
				}
			}
		}
		catch (Exception ex)
		{
			_cmdCtrl.State["Action"] = "Error";
			_cmdCtrl.State["Exception"] = ex;
		}
	}

	protected String RenderFriendList()
	{
		AccountInfo cu = AccountImpl.Instance.GetUserInfo(ServerImpl.Instance.GetUserName(Context));
		StringBuilder builder = new StringBuilder();
		foreach (string name in AccountImpl.Instance.GetUserInfo(cu.Name).Friends)
		{
			AccountInfo fi = AccountImpl.Instance.GetUserInfo(name);
			if (fi.Type == 1 && fi.Name.ToLower() != "sa")
			{
				AccountInfo createor = AccountImpl.Instance.GetUserInfo(fi.Creator);
				builder.AppendFormat(
					RowFormat, "",
					HtmlUtil.ReplaceHtml(fi.Name),
					HtmlUtil.ReplaceHtml(fi.Nickname),
					HtmlUtil.ReplaceHtml(createor.Nickname),
					createor.Name,
					fi.ID,
					Rookey.Frame.IMCore.Utility.RenderJson(fi.Nickname),
					Rookey.Frame.IMCore.Utility.RenderJson(fi.Name),
					Rookey.Frame.IMCore.Utility.RenderJson(cu.ID == createor.ID),
					cu.ID == createor.ID ? "解散群组" : "退出群组",
					cu.ID == createor.ID ? String.Format("<a href='javascript:Update(\"{0}\")'>修改群资料</a>", HtmlUtil.ReplaceHtml(fi.Name)) : ""
				);
			}
		}
		return builder.ToString();
	}
}
