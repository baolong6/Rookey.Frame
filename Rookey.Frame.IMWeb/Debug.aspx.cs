﻿using Rookey.Frame.IMCore;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class Debug : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}
}


public class TestCommandHandler : CommandHandler
{
	public TestCommandHandler(HttpContext context, String sessionId, String id, String data) :
		base(context, sessionId, id, data)
	{
	}

	public override void Process(object data)
	{
		int Count = (int)(Double)Rookey.Frame.IMCore.Utility.ParseJson(Data);
		Rookey.Frame.IMCore.SessionManagement.Instance.Send(UserName, CommandID, Rookey.Frame.IMCore.Utility.RenderJson(Count));

	}

	public override string Process()
	{
		throw new NotImplementedException();
	}
}