﻿/*----------------------------------------------------------------
        // Copyright (C) 2016 Rookey
        // 版权所有
        // 开发者：Rookey
        // Email：rookey@yeah.net
        // QQ：3319549098
//----------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rookey.Frame.Operate.Base.TempModel
{
    /// <summary>
    /// 附件信息
    /// </summary>
    public class AttachFileInfo
    {
        public string Id { get; set; }
        public string AttachFile { get; set; }
        public string PdfFile { get; set; }
        public string SwfFile { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string FileDes { get; set; }
    }

}
