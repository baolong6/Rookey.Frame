﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Rookey.Frame.IMCore
{
	public interface IAccountStorage
	{
		DataRow GetAccountInfo(string name);
		DataRow GetAccountInfo(Int64 key);

		DataRowCollection GetAllUsers();
		DataRowCollection GetAllGroups();
		String[] GetGroupManagers(string name);

		DataRowCollection GetFriends(string name);
		Int64 GetRelationship(string account1, string account2);
		bool Validate(string userId, string password);
		String[] GetUserRoles(string userId);

		void AddFriend(string user, string friend);
		void DeleteFriend(Int64 user, Int64 friend);
		void DeleteUser(Int64 name);
		void DeleteGroup(Int64 id);

		void CreateUser(String name, String nickname, String password, String email);
        void CreateTempUser(string name, string nickname);
		void CreateGroup(String creator, String name, String nickname);

		void UpdateUserInfo(string name, Hashtable values);
	}
}
